﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labIfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            AAA(5);
            AAA(25);
        }
        private static void AAA(int v)
        {
            if (v < 15)
            {
                Console.WriteLine("{0} меньше 15", v);
            }
            else if (v < 20)
            {
                Console.WriteLine("{0} меньше 20", v);
            }
            else
            {
                Console.WriteLine("{0} больше 20", v);
            }
        }

    }
}




