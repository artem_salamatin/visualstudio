﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labCyrcles
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1, 2, 3, 4 };
            int r = 0;
            foreach (int i in x)
            {
                r = r + i;
                Console.WriteLine(i);
            }
            Console.WriteLine();
            Console.WriteLine("sum = {0}", r);
        }
    }
}
